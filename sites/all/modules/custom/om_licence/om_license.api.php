<?php
/**
 * @file
 * om_license.api.php
 */

/**
 * Implements hook_om_license_module_info().
 *
 * @return
 *  array of module info.
 */
function hook_om_license_module_info() {
  return array(
    'om_basecamp' => array(
      'name' => 'OM Basecamp',
      'machine_name' => 'om_basecamp'
    )
  );
}
