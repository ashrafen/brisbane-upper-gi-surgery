<?php

/**
 * Implements template_preprocess_html().
 */
function omtheme_preprocess_html(&$variables) {
}

/**
 * Implements template_preprocess_page.
 */
function omtheme_preprocess_page(&$variables) {

    if (!empty($variables['page']['sidebar_first'])){
        $left = $variables['page']['sidebar_first'];
    }

    if (!empty($variables['page']['sidebar_second'])) {
        $right = $variables['page']['sidebar_second'];
    }
// Dynamic sidebars
    if (!empty($left) && !empty($right)) {
        $variables['main_grid'] = 'large-3 large-push-3';
        $variables['sidebar_first_grid'] = 'large-3 large-pull-3';
        $variables['sidebar_sec_grid'] = 'large-3';
    } elseif (empty($left) && !empty($right)) {
        $variables['main_grid'] = 'large-7 large-offset-1 medium-12 medium-offset-0';
        $variables['sidebar_first_grid'] = '';
        $variables['sidebar_sec_grid'] = 'large-3';
    } elseif (!empty($left) && empty($right)) {
        $variables['main_grid'] = 'large-8 large-push-3';
        $variables['sidebar_first_grid'] = 'large-3 large-pull-8';
        $variables['sidebar_sec_grid'] = '';
    } else {
        $variables['main_grid'] = 'large-11 large-offset-1 medium-12 medium-offset-0';
        $variables['sidebar_first_grid'] = '';
        $variables['sidebar_sec_grid'] = '';
    }

}

/**
 * Implements template_preprocess_node.
 */
function omtheme_preprocess_node(&$variables) {
}
